using System;

namespace Csharp
{
    class zad1
    {
        static void Main(string[] args)
        {
            float x = 1;
            float suma = 0;
            do
            {
                if(x%2!=0)
                    suma += x;
                x++;
            } while (x <= 100);
            Console.WriteLine(suma);
        }
    }
}